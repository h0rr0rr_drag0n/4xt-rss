"""Daemon for 4xt-rss program.

This daemon updates feeds contents in database and provide
access to database via REST API.
"""

import bottle
import configparser
import logging
import multiprocessing
import os
import sys

import rssd4.worker.worker

from rssd4.api.routes import Categories, Feeds


def read_settings(config_filename='/etc/rssd4.conf'):
    settings = {}
    config = configparser.ConfigParser()

    if os.path.exists(config_filename) is False:
        print('Configuration file %s does not exists!' % config_filename,
              file=sys.stderr)
        raise FileNotFoundError('No configuration file!')
    config.read(config_filename)

    settings['log_file'] = config['GENERAL']['log_file']

    settings['ip_address'] = config['NETWORK']['ip_address']
    settings['port'] = config['NETWORK']['port']
    settings['login'] = config['NETWORK']['login']
    settings['password'] = config['NETWORK']['password']

    settings['database_directory'] = config['DB']['database_directory']
    settings['h_settings_db'] = config['DB']['harvesters_settings_db']
    settings['data_db'] = config['DB']['data_db']

    settings['harvester_delay'] = config.getint('DELAYS', 'harvester_delay')
    settings['update_delay'] = config.getint('DELAYS', 'update_delay') * 60

    return settings


def start_worker(lock, settings):
    rssd4.worker.worker.main(lock, settings)


def start_api(lock, args, settings):
    lock.acquire()
    logging.debug("Lock acquired in API process")
    categories = Categories(
        settings['login'],
        settings['password'],
        settings['database_directory'],
        settings['h_settings_db'])
    feeds = Feeds(
        settings['login'],
        settings['password'],
        settings['database_directory'],
        settings['h_settings_db'],
        settings['data_db'])
    bottle.debug(args.debug)
    lock.release()
    logging.debug("Lock released in API process")
    if args.standalone is True:
        bottle.run(host=settings['ip_address'], port=settings['port'])
        return None
    else:
        return bottle.default_app()


def start_main_program(args, settings):
    worker_lock = multiprocessing.Lock()
    worker_lock.acquire()

    logging_settings = {
        'format': '%(asctime)s:%(levelname)s:%(message)s',
        'datefmt': '%Y-%m-%dT%H:%M:%S%z',
        'filename': settings['log_file']
    }
    if args.debug is True:
        logging_settings.update({'level': logging.DEBUG})
    else:
        logging_settings.update({'level': logging.INFO})
    logging.basicConfig(**logging_settings)
    logging.info("Starting rssd4 daemon...")

    worker_process = multiprocessing.Process(target=start_worker,
                                             args=(worker_lock, settings))
    worker_process.daemon = True
    worker_process.start()
    logging.debug("Started RSSD4 worker process (PID: %s)" % worker_process.pid)
    return start_api(worker_lock, args, settings)
