"""Low-level API functions, which is used by program API
(HTTP API, for example).
"""

import logging
import os
import sqlite3


# Functions to work with settings table.

class HarvestersSettings:
    """Class to operate with harvester settings."""

    def __init__(self, db_dir, db_name):
        if os.path.exists(db_dir + db_name) is False:
            logging.error('Database %s not found!' % (db_dir + db_name,))
            raise FileNotFoundError('No settings database found')
        self.connection = sqlite3.connect(database=db_dir + db_name,
                                          check_same_thread=False)
        cursor = self.connection.cursor()
        cursor.execute('PRAGMA journal_mode=WAL;')
        self.connection.commit()
        cursor.close()

    def add_new_feed(self, category, feed_name, url, harvester_type):
        """Adding new feed to the harvester database.

        Parameters:
         category - name of category
         feed_name - name of feed, which we want to add
         url - URL for the harvester (for example - URL to RSS feed for RSS
               harvester.
         harvester_type - type of harvester (for example 'rss' for RSS
                          harvester.
        """
        insert_statement = 'INSERT INTO {tablename} ({columns}) VALUES(?, ?, ?, ?);'.format(
            tablename='harvesters_settings',
            columns='category, feed_name, url, harvester_type')
        cursor = self.connection.cursor()
        cursor.execute(insert_statement, (category, feed_name, url,
                                          harvester_type))
        self.connection.commit()
        cursor.close()
        logging.debug('Added new item to harvester settings: %s %s %s %s' % (
            category, feed_name, url, harvester_type))

    def get_categories_feeds_list(self):
        """Returns list of feed ID, category and feed name.

        Result:
         Iterable of iterables like that: (feed_id, category, feed_name).
        """
        select_statement = 'SELECT feed_id, category, feed_name FROM ' \
                           'harvesters_settings;'
        cursor = self.connection.cursor()
        select_result = cursor.execute(select_statement)
        for result_item in select_result:
            yield result_item
        cursor.close()

    def edit_category_name(self, old_name, new_name):
        """Changes category name.
        Parameters:
         old_name - old category name
         new_name - new category name
        """
        update_statement = "UPDATE OR ROLLBACK harvesters_settings " \
                           "SET category='{new_name}' " \
                           "WHERE category='{old_name}';".format(
                                old_name=old_name,
                                new_name=new_name)
        cursor = self.connection.cursor()
        cursor.execute(update_statement)
        self.connection.commit()
        cursor.close()

    def edit_feed_name(self, feed_id, new_name):
        """Changes item name.
        Parameters:
         feed_id - ID of the necessary feed
         new_name - new name for the feed
        """
        update_statement = "UPDATE OR ROLLBACK harvesters_settings " \
                           "SET feed_name='{new_name}' " \
                           "WHERE feed_id='{feed_id}';".format(
                                new_name=new_name,
                                feed_id=feed_id)
        cursor = self.connection.cursor()
        cursor.execute(update_statement)
        self.connection.commit()
        cursor.close()

    def delete_feed(self, feed_id):
        """Deletes an feed with given feed id."""
        delete_statement = 'DELETE FROM harvesters_settings ' \
                           'WHERE feed_id={};'.format(feed_id)
        cursor = self.connection.cursor()
        cursor.execute(delete_statement)
        self.connection.commit()
        cursor.close()

    def delete_category(self, category_name):
        """Deletes all feeds and category itself from harvesters settings
        database.
        """
        delete_statement = "DELETE FROM harvesters_settings " \
                           "WHERE category='{}';".format(category_name)
        cursor = self.connection.cursor()
        cursor.execute(delete_statement)
        self.connection.commit()
        cursor.close()


# Functions to work with data table.

class HarvestersData:
    """Class to operate with data, retrieved via different Harvesters"""

    def __init__(self, db_dir, db_name):
        if os.path.exists(db_dir + db_name) is False:
            logging.error('Database %s not found!' % (db_dir + db_name,))
            raise FileNotFoundError('No data database found')
        self.connection = sqlite3.connect(database=db_dir + db_name,
                                          check_same_thread=False)
        cursor = self.connection.cursor()
        cursor.execute('PRAGMA journal_mode=WAL;')
        self.connection.commit()
        cursor.close()

    def get_all_items(self, feed_id):
        """Returns iterator with items from the database."""
        select_statement = 'SELECT rowid, datetime, author, header, body, new ' \
                           'FROM data WHERE feed_id={};'.format(feed_id)
        cursor = self.connection.cursor()
        select_result = cursor.execute(select_statement)
        for result_item in select_result:
            yield result_item
        cursor.close()

    def get_unread_items(self, feed_id):
        """Return iterator with unread items from database."""
        select_statement = 'SELECT rowid, datetime, author, header, body, new ' \
                           'FROM unread_data WHERE feed_id={}'.format(feed_id)
        cursor = self.connection.cursor()
        select_result = cursor.execute(select_statement)
        for result_item in select_result:
            yield result_item
        cursor.close()

    def set_item_as_read(self, rowid):
        """Marks row from data table as read."""
        update_statement = "UPDATE OR ROLLBACK data SET new=0 WHERE " \
                           "rowid={}".format(rowid)
        cursor = self.connection.cursor()
        cursor.execute(update_statement)
        self.connection.commit()
        cursor.close()
