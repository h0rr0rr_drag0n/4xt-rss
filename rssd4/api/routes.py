import bottle

from rssd4.api.functions import HarvestersData, HarvestersSettings


class Route:
    route_prefix = '/api/v1.0'
    route_table = {}

    def __init__(self, login, password):
        """Parameters:
         login - login to access to Web API.
         password - password to access to Web API.
        """
        self.initialize_route_table()
        for ((route, method), function) in self.route_table.items():
            bottle.route(self.route_prefix + route, method, function)
        self.login = login
        self.password = password

    def initialize_route_table(self):
        pass

    @staticmethod
    def _api_auth(fn):
        def check_credentials(self, *args, **kwargs):
            if bottle.request.auth == (self.login, self.password):
                return fn(self, *args, **kwargs)
            else:
                bottle.abort(401, 'Unauthorized')
        return check_credentials


class Categories(Route):
    def __init__(self, login, password, db_dir, db_name):
        self.db_dir = db_dir
        self.db_name = db_name
        super(Categories, self).__init__(login, password)

    def initialize_route_table(self):
        self.route_table = {
            ('/categories/<category_name>', 'POST'): self.edit_category_name,
            ('/categories/<category_name>', 'DELETE'): self.delete_category
        }

    @Route._api_auth
    def edit_category_name(self, category_name):
        hs = HarvestersSettings(self.db_dir, self.db_name)
        hs.edit_category_name(old_name=category_name,
                              new_name=bottle.request.json['new_name'])

    @Route._api_auth
    def delete_category(self, category_name):
        hs = HarvestersSettings(self.db_dir, self.db_name)
        hs.delete_category(category_name)


class Feeds(Route):
    def __init__(self, login, password, db_dir, settings_db_name, data_db_name):
        self.db_dir = db_dir
        self.settings_db_name = settings_db_name
        self.data_db_name = data_db_name
        super(Feeds, self).__init__(login, password)

    def initialize_route_table(self):
        self.route_table = {
            ('/feeds', 'GET'): self.categories_feeds_list,
            ('/feeds', 'POST'): self.add_new_feed,
            ('/feeds/<feed_id>', 'POST'): self.edit_feed_name,
            ('/feeds/<feed_id>', 'DELETE'): self.delete_feed,
            ('/feeds/<feed_id>/items', 'GET'): self.get_all_items,
            ('/feeds/<feed_id>/unread_items', 'GET'): self.get_unread_items,
            ('/feeds/<feed_id>/items/<item_id>', 'POST'): self.set_item_as_read
        }

    @Route._api_auth
    def categories_feeds_list(self):
        result = {'feeds_list': []}
        hs = HarvestersSettings(self.db_dir, self.settings_db_name)
        for feed_item in hs.get_categories_feeds_list():
            result['feeds_list'].append({
                'feed_id': feed_item[0],
                'category': feed_item[1],
                'feed_name': feed_item[2]
            })
        return result

    @Route._api_auth
    def add_new_feed(self):
        hs = HarvestersSettings(self.db_dir, self.settings_db_name)
        hs.add_new_feed(
            bottle.request.json['category'],
            bottle.request.json['feed_name'],
            bottle.request.json['url'],
            bottle.request.json['harvester_type'])

    @Route._api_auth
    def edit_feed_name(self, feed_id):
        hs = HarvestersSettings(self.db_dir, self.settings_db_name)
        hs.edit_feed_name(feed_id, bottle.request.json['new_name'])

    @Route._api_auth
    def delete_feed(self, feed_id):
        hs = HarvestersSettings(self.db_dir, self.settings_db_name)
        hs.delete_feed(feed_id)

    def get_items(self, feed_id, item_getter):
        result = {'items_list': []}
        for item in item_getter(feed_id):
            result['items_list'].append({
                'id': item[0],
                'datetime': item[1],
                'author': item[2],
                'header': item[3],
                'body': item[4],
                'new': item[5]
            })
        return result

    @Route._api_auth
    def get_all_items(self, feed_id):
        hd = HarvestersData(self.db_dir, self.data_db_name)
        return self.get_items(feed_id, hd.get_all_items)

    @Route._api_auth
    def get_unread_items(self, feed_id):
        hd = HarvestersData(self.db_dir, self.data_db_name)
        return self.get_items(feed_id, hd.get_unread_items)

    @Route._api_auth
    def set_item_as_read(self, feed_id, item_id):
        hd = HarvestersData(self.db_dir, self.data_db_name)
        hd.set_item_as_read(item_id)
