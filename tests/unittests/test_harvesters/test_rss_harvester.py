"""Tests for harvesters/rss_harvester.py module."""

import datetime
import unittest

from unittest.mock import patch
from unittest.mock import MagicMock

from rssd4.harvesters.rss_harvester import RSSHarvester


class ExecOnceTestCase(unittest.TestCase):
    """Test for exec_once() method."""

    def setUp(self):
        with patch('rssd4.harvesters.rss_harvester.Harvester.__init__') as mock_init:
            mock_init.return_value = None
            self.harvester = RSSHarvester(1, 'test_url', 'test_dir', 'test_name')
            self.harvester.url = 'test_url'

    @patch('rssd4.harvesters.rss_harvester.feedparser')
    def test_normal_operation(self, mock_feedparser):
        """Method should runs without any side effects."""
        with patch('rssd4.harvesters.rss_harvester.Harvester.exec_once'):
            self.harvester.exec_once()


class GetNewDataTestCase(unittest.TestCase):
    """Test for get_new_data() function."""

    def setUp(self):
        with patch('rssd4.harvesters.rss_harvester.Harvester.__init__') as mock_init:
            mock_init.return_value = None
            self.harvester = RSSHarvester(1, 'test_url', 'test_dir', 'test_name')

    def test_normal_operation(self):
        """Method should return generator with feeds' items without any
        side effects.
        """
        test_value = MagicMock()
        test_value.published = 'Mon, 12 Oct 2012 11:02:33 +0800'
        test_value.author = 'test_author'
        test_value.title = 'test_title'
        test_value.summary = 'test_summary'

        self.harvester.rss_feed = MagicMock()
        self.harvester.rss_feed.entries = [test_value, ]
        result = self.harvester.get_new_data()
        self.assertTupleEqual(result.__next__(),
                              (
                                  datetime.datetime.strptime(
                                      'Mon, 12 Oct 2012 11:02:33 +0800',
                                      '%a, %d %b %Y %H:%M:%S %z'),
                                  'test_author',
                                  'test_title',
                                  'test_summary'
                              ))

    def test_timezone_abbreviation(self):
        """Method should parse given timezone abbreviation and return
        generator without any side effects.
        """
        test_value = MagicMock()
        test_value.published = 'Mon, 12 Oct 2012 11:02:33 TEST'
        test_value.author = 'test_author'
        test_value.title = 'test_title'
        test_value.summary = 'test_summary'

        self.harvester.rss_feed = MagicMock()
        self.harvester.rss_feed.entries = [test_value, ]
        with patch('rssd4.harvesters.rss_harvester.tz') as mock_tz:
            mock_tz.gettz.return_value = None
            result = self.harvester.get_new_data()
        self.assertTupleEqual(result.__next__(),
                              (
                                  datetime.datetime.strptime(
                                      '2012-10-12T11:02:33',
                                      '%Y-%m-%dT%H:%M:%S'),
                                  'test_author',
                                  'test_title',
                                  'test_summary'
                              ))

    def test_no_author(self):
        """If there is no author in the feed's item, method should use empty string
        as author name and return generator without any side effects.
        """
        class TestValue:
            published = 'Mon, 12 Oct 2012 11:02:33 +0800'
            title = 'test_title'
            summary = 'test_summary'
        test_value = TestValue()

        self.harvester.rss_feed = MagicMock()
        self.harvester.rss_feed.entries = [test_value, ]
        result = self.harvester.get_new_data()
        self.assertTupleEqual(result.__next__(),
                              (
                                  datetime.datetime.strptime(
                                      'Mon, 12 Oct 2012 11:02:33 +0800',
                                      '%a, %d %b %Y %H:%M:%S %z'),
                                  '',
                                  'test_title',
                                  'test_summary'
                              ))


if __name__ == '__main__':
    unittest.main()
