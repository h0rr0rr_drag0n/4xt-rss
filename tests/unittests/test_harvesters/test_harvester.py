"""Unit-tests for harvesters/harvester.py module."""

import datetime
import unittest

from unittest.mock import patch
from unittest.mock import MagicMock

from rssd4.harvesters.harvester import Harvester


class HarvesterConstructorTestCase(unittest.TestCase):
    """Test for Harvester's constructor."""

    @patch('rssd4.harvesters.harvester.sqlite3')
    def test_constructor(self, mock_sqlite3):
        """Constructor should check database existence, execute
        all necessary SQL statements and quit.
        """
        mock_connection = MagicMock()
        mock_sqlite3.connect.return_value = mock_connection
        with patch('rssd4.harvesters.harvester.logging'), \
                patch('rssd4.harvesters.harvester.Harvester'
                      '.check_db_existence') as check_db:
            Harvester(1, '', '', '')
            check_db.assert_called_once_with()


class CheckDbExistenceTestCase(unittest.TestCase):
    """Tests for check_db_existence() method."""

    def setUp(self):
        with patch('rssd4.harvesters.harvester.sqlite3'), \
                patch('rssd4.harvesters.harvester.logging'), \
                patch('rssd4.harvesters.harvester.Harvester'
                      '.check_db_existence'):
            self.harvester = Harvester(1, 'test_url', 'test_dir', 'test_name')

    @patch('rssd4.harvesters.harvester.os')
    def test_db_exists(self, mock_os):
        """Method should returns without any side effects
        if database file exists.
        """
        mock_os.path.isdir.return_value = True
        mock_os.path.exists.return_value = True
        self.harvester.check_db_existence()

    @patch('rssd4.harvesters.harvester.open', create=True)
    @patch('rssd4.harvesters.harvester.os')
    def test_db_file_not_exists(self, mock_os, mock_open):
        """If path to database exists and database file does
        not exists - method should create database file
        via open() call.
        """
        mock_os.path.isdir.return_value = True
        mock_os.path.exists.return_value = False
        with patch('rssd4.harvesters.harvester.logging'):
            self.harvester.check_db_existence()
        mock_open.assert_called_once_with('test_dirtest_name', 'w')

    @patch('rssd4.harvesters.harvester.open', create=True)
    @patch('rssd4.harvesters.harvester.os')
    def test_db_path_not_exists(self, mock_os, mock_open):
        """If path to database does not exists, method should
        create path to database file and create a database file.
        """
        mock_os.path.isdir.return_value = False
        mock_os.path.exists.return_value = False
        with patch('rssd4.harvesters.harvester.logging'):
            self.harvester.check_db_existence()
        mock_os.makedirs.assert_called_once_with('test_dir')
        mock_open.assert_called_once_with('test_dirtest_name', 'w')

    @patch('rssd4.harvesters.harvester.os')
    def test_db_dir_exists_as_file(self, mock_os):
        """If path to database exists but it is ends with file, then
        method should raise FileExistsError.
        """
        mock_os.path.isdir.return_value = False
        mock_os.path.exists.return_value = True
        with patch('rssd4.harvesters.harvester.logging'):
            self.assertRaises(FileExistsError,
                              self.harvester.check_db_existence)


class ExecOnceTestCase(unittest.TestCase):
    """Test for exec_once() method."""

    def setUp(self):
        with patch('rssd4.harvesters.harvester.sqlite3'), \
             patch('rssd4.harvesters.harvester.logging'), \
             patch('rssd4.harvesters.harvester.Harvester'
                   '.check_db_existence'):
            self.harvester = Harvester(1, 'test_url', 'test_dir', 'test_name')

    def test_normal_operation(self):
        """Method should runs without any side effects."""
        with patch('rssd4.harvesters.harvester.Harvester'
                   '.get_latest_save_time'), \
                patch('rssd4.harvesters.harvester.Harvester'
                      '.write_to_database'), \
                patch('rssd4.harvesters.harvester.Harvester'
                      '.get_new_data'):
            self.harvester.exec_once()


class WriteToDatabaseTestCase(unittest.TestCase):
    """Test for write_to_database() method."""

    def setUp(self):
        with patch('rssd4.harvesters.harvester.sqlite3'), \
             patch('rssd4.harvesters.harvester.logging'), \
             patch('rssd4.harvesters.harvester.Harvester'
                   '.check_db_existence'):
            self.harvester = Harvester(1, 'test_url', 'test_dir', 'test_name')

    def test_normal_operation(self):
        """Method should runs without any side effects."""
        input_value = (
            (datetime.datetime.now(), '', '', ''),
        )
        with patch('rssd4.harvesters.harvester.Harvester'
                   '.connection', create=True), \
                patch('rssd4.harvesters.harvester.logging'):
            self.harvester.write_to_database(input_value)


class GetLatestSaveTimeTestCase(unittest.TestCase):
    """Tests for get_latest_save_time() method."""

    def setUp(self):
        with patch('rssd4.harvesters.harvester.sqlite3'), \
             patch('rssd4.harvesters.harvester.logging'), \
             patch('rssd4.harvesters.harvester.Harvester'
                   '.check_db_existence'):
            self.harvester = Harvester(1, 'test_url', 'test_dir', 'test_name')

    def test_normal_operation(self):
        """Method should return result from database in ISO8601 format."""
        mock_cursor = MagicMock()
        mock_datetime_rows = MagicMock()
        self.mock_latest_time = ['2010-02-10T11:22:33+0300', ]
        self.harvester.connection.cursor.return_value = mock_cursor
        mock_cursor.execute.return_value = mock_datetime_rows
        mock_datetime_rows.fetchone.return_value = self.mock_latest_time

        expected_value = datetime.datetime.strptime(
            '2010-02-10T11:22:33+0300', '%Y-%m-%dT%H:%M:%S%z')
        result = self.harvester.get_latest_save_time()
        self.assertEqual(result, expected_value)

    def test_no_latest_time(self):
        """Method should return start of Unix epoch date and time in
        ISO8601 format.
        """
        mock_cursor = MagicMock()
        mock_datetime_rows = MagicMock()
        self.mock_latest_time = [None, ]
        self.harvester.connection.cursor.return_value = mock_cursor
        mock_cursor.execute.return_value = mock_datetime_rows
        mock_datetime_rows.fetchone.return_value = self.mock_latest_time

        expected_value = datetime.datetime.strptime(
            '1970-01-01T00:00:00+0000', '%Y-%m-%dT%H:%M:%S%z')
        result = self.harvester.get_latest_save_time()
        self.assertEqual(result, expected_value)


if __name__ == '__main__':
    unittest.main()
