"""Tests for api/routes.py module."""

import unittest

from unittest.mock import patch
from unittest.mock import MagicMock

from rssd4.api.routes import Categories
from rssd4.api.routes import Feeds
from rssd4.api.routes import Route


class RouteTestCase(unittest.TestCase):
    """Unittests for methods of Route class."""

    def test_constructor(self):
        """Constructor should runs without any side effects."""
        with patch('rssd4.api.routes.bottle'):
            Route('test_login', 'test_password')

    def test_api_auth_normal(self):
        """Decorator should return fn result if authentication is passed."""
        expected_result = 'test passed'
        mock_self = MagicMock()
        mock_self.login = 'test1'
        mock_self.password = 'test2'

        def fn(self):
            return expected_result

        with patch('rssd4.api.routes.bottle') as patch_bottle:
            patch_bottle.request.auth = ('test1', 'test2')
            decorated_function = Route('test1', 'test2')._api_auth(fn)
            result = decorated_function(mock_self)
        self.assertEqual(expected_result, result)

    def test_api_auth_wrong_credentials(self):
        """Decorator should call abort() if authentication is failed."""
        mock_self = MagicMock()
        mock_self.login = 'test1'
        mock_self.password = 'test2'

        def fn(self):
            pass

        with patch('rssd4.api.routes.bottle') as patch_bottle:
            patch_bottle.request.auth = ('test3', 'test4')
            decorated_function = Route('test1', 'test2')._api_auth(fn)
            decorated_function(mock_self)
            patch_bottle.abort.assert_called_once_with(401, 'Unauthorized')


class CategoriesTestCase(unittest.TestCase):
    """Unittests for methods of Categories class."""

    def test_constructor(self):
        """Constructor should runs without any side effects."""
        with patch('rssd4.api.routes.bottle'):
            Categories('', '', '', '')

    def test_initialize_route_table(self):
        """Method initialize_route_table() should runs without
        any side effects.
        """
        with patch('rssd4.api.routes.bottle'):
            Categories('', '', '', '').initialize_route_table()

    def test_edit_category_name(self):
        """Method edit_category_name() should runs without
        any side effects.
        """
        with patch('rssd4.api.routes.bottle') as patch_bottle, \
                patch('rssd4.api.routes.HarvestersSettings'):
            patch_bottle.request.auth = ('', '')
            Categories('', '', '', '').edit_category_name('')

    def test_delete_category(self):
        """Method delete_category() should runs without any
        side effects.
        """
        with patch('rssd4.api.routes.bottle') as patch_bottle, \
                patch('rssd4.api.routes.HarvestersSettings'):
            patch_bottle.request.auth = ('', '')
            Categories('', '', '', '').delete_category('')


class FeedsTestCase(unittest.TestCase):
    """Unittests for methods of Feeds class."""

    def test_constructor(self):
        """Constructor should runs without any side effects."""
        with patch('rssd4.api.routes.bottle'):
            Feeds('', '', '', '', '')

    def test_initialize_route_table(self):
        """Method initialize_route_table() should runs
        without any side effects.
        """
        with patch('rssd4.api.routes.bottle'):
            Feeds('', '', '', '', '').initialize_route_table()

    def test_categories_feeds_list(self):
        """Method categories_feeds_list() should runs without
        any side effects.
        """
        with patch('rssd4.api.routes.bottle') as patch_bottle, \
                patch('rssd4.api.routes.HarvestersSettings'):
            patch_bottle.request.auth = ('', '')
            Feeds('', '', '', '', '').categories_feeds_list()

    def test_add_new_feed(self):
        """Method add_new_feed() should runs without
        any side effects.
        """
        with patch('rssd4.api.routes.bottle') as patch_bottle, \
                patch('rssd4.api.routes.HarvestersSettings'):
            patch_bottle.request.auth = ('', '')
            Feeds('', '', '', '', '').add_new_feed()

    def test_edit_feed_name(self):
        """Method edit_feed_name() should runs without any side
        effects.
        """
        with patch('rssd4.api.routes.bottle') as patch_bottle, \
                patch('rssd4.api.routes.HarvestersSettings'):
            patch_bottle.request.auth = ('', '')
            Feeds('', '', '', '', '').edit_feed_name(1)

    def test_delete_feed(self):
        """Method delete_feed() should runs without any side effects."""
        with patch('rssd4.api.routes.bottle') as patch_bottle, \
                patch('rssd4.api.routes.HarvestersSettings'):
            patch_bottle.request.auth = ('', '')
            Feeds('', '', '', '', '').delete_feed(1)

    def test_get_items(self):
        """Method get_items() should runs without any side effects."""
        mock_item_getter = MagicMock()
        Feeds('', '', '', '', '').get_items(1, mock_item_getter)

    def test_get_all_items(self):
        """Method get_all_items() should runs without
        any side effects.
        """
        with patch('rssd4.api.routes.bottle') as patch_bottle, \
                patch('rssd4.api.routes.HarvestersData'):
            patch_bottle.request.auth = ('', '')
            Feeds('', '', '', '', '').get_all_items(1)

    def test_get_unread_items(self):
        """Method get_unread_items() should runs without
        any side effects.
        """
        with patch('rssd4.api.routes.bottle') as patch_bottle, \
                patch('rssd4.api.routes.HarvestersData'):
            patch_bottle.request.auth = ('', '')
            Feeds('', '', '', '', '').get_unread_items(1)

    def test_set_item_as_read(self):
        """Method set_item_as_read() should runs without any
        side effects.
        """
        with patch('rssd4.api.routes.bottle') as patch_bottle, \
                patch('rssd4.api.routes.HarvestersData'):
            patch_bottle.request.auth = ('', '')
            Feeds('', '', '', '', '').set_item_as_read(1, 1)


if __name__ == '__main__':
    unittest.main()
