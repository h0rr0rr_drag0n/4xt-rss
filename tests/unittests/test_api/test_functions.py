"""Tests for api/functions.py module."""

import unittest

from unittest.mock import patch

from rssd4.api.functions import HarvestersData
from rssd4.api.functions import HarvestersSettings


class HarvestersSettingsConstructorTestCase(unittest.TestCase):
    """Tests for HarvestersSettings constructor."""

    @patch('rssd4.api.functions.os')
    def test_normal_operation(self, mock_os):
        """Method should returns without any side effects if
         database file exists.
         """
        mock_os.path.exists.return_value = True
        with patch('rssd4.api.functions.sqlite3'):
            HarvestersSettings('', '')

    @patch('rssd4.api.functions.os')
    def test_no_database_file(self, mock_os):
        """Method should write to log and raise FileNotFoundError
        if database file does not exists.
        """
        mock_os.path.exists.return_value = False
        with patch('rssd4.api.functions.logging') as mock_logging:
            self.assertRaises(FileNotFoundError, HarvestersSettings,
                              'test_db_dir', 'test_db_name')
            mock_logging.error.assert_called_once_with('Database test_db_dir'
                                                       'test_db_name not found!')


class HarvestersSettingsMethodsTestCase(unittest.TestCase):
    """Tests for all HarvestersSettings methods."""

    def setUp(self):
        with patch('rssd4.api.functions.sqlite3'), \
                patch('rssd4.api.functions.os') as mock_os:
            mock_os.path.exists.return_value = True
            self.harvesters_settings = HarvestersSettings('test_dir', 'test_name')

    def test_add_new_feed(self):
        """Method should returns without any side effects."""
        with patch('rssd4.api.functions.logging'):
            self.harvesters_settings.add_new_feed('', '', '', '')

    def test_get_categories_feeds_list(self):
        """Method should returns iterable object without any
        side effects.
        """
        result = self.harvesters_settings.get_categories_feeds_list()
        self.assertTrue(hasattr(result, '__next__'))

    def test_edit_category_name(self):
        """Method should returns without any side effects."""
        self.harvesters_settings.edit_category_name('', '')

    def test_edit_feed_name(self):
        """Method should returns without any side effects."""
        self.harvesters_settings.edit_feed_name(1, '')

    def test_delete_feed(self):
        """Method should returns without any side effects."""
        self.harvesters_settings.delete_feed(1)

    def test_delete_category(self):
        """Method should returns without any side effects."""
        self.harvesters_settings.delete_category('')


class HarvestersDataConstructorTestCase(unittest.TestCase):
    """Tests for HarvestersData constructor."""

    @patch('rssd4.api.functions.os')
    def test_normal_operation(self, mock_os):
        """Method should returns without any side effects
        if database file exists.
        """
        mock_os.path.exists.return_value = True
        with patch('rssd4.api.functions.sqlite3'):
            HarvestersData('', '')

    @patch('rssd4.api.functions.os')
    def test_no_database_file(self, mock_os):
        """Method should raise FileNotFoundError if
        database file not exists.
        """
        mock_os.path.exists.return_value = False
        with patch('rssd4.api.functions.logging') as mock_logging:
            self.assertRaises(FileNotFoundError, HarvestersData,
                              'test_dir', 'test_name')
            mock_logging.error.assert_called_once_with(
                'Database test_dirtest_name not found!')


class HarvestersDataMethodsTestCase(unittest.TestCase):
    """Tests for all HarvestersData methods."""

    def setUp(self):
        with patch('rssd4.api.functions.sqlite3'), \
                patch('rssd4.api.functions.os') as mock_os:
            mock_os.path.exists.return_value = True
            self.harvesters_data = HarvestersData('test_dir', 'test_name')

    def test_get_all_items(self):
        """Method should returns iterator and exit without
        any side effects."""
        result = self.harvesters_data.get_all_items(1)
        self.assertTrue(hasattr(result, '__next__'))

    def test_get_unread_items(self):
        """Method should return iterator and exit without
        any side effects."""
        result = self.harvesters_data.get_unread_items(1)
        self.assertTrue(hasattr(result, '__next__'))

    def test_set_item_as_read(self):
        """Method should returns without any side effects."""
        self.harvesters_data.set_item_as_read(1)


if __name__ == '__main__':
    unittest.main()
