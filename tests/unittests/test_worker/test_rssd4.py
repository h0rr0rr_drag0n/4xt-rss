"""Unit-tests for worker/rssd4.py module."""

import unittest

from unittest.mock import patch
from unittest.mock import MagicMock

from rssd4.worker.rssd4 import read_settings
from rssd4.worker.rssd4 import start_api
from rssd4.worker.rssd4 import start_main_program
from rssd4.worker.rssd4 import start_worker


class ReadSettingsTestCase(unittest.TestCase):
    """Tests for read_settings() function."""

    @patch('rssd4.worker.rssd4.configparser.ConfigParser')
    @patch('rssd4.worker.rssd4.os.path.exists')
    def test_normal_operation(self, exists, configparser):
        """Test what read_settings() return valid settings from existing
        configuration file.
        """
        given_config = {
            'GENERAL': {
                'log_file': 'test_log_file_path'
            },
            'NETWORK': {
                'ip_address': 'test_ip_address',
                'port': 'test_port',
                'login': 'test_login',
                'password': 'test_password'
            },
            'DB': {
                'database_directory': 'test_db_directory_path',
                'harvesters_settings_db': 'test_settings_database',
                'data_db': 'test_database'
            },
            'DELAYS': {
                'harvester_delay': 10,
                'update_delay': 5
            }
        }
        config = MagicMock()
        configparser.return_value = config
        config.__getitem__ = self.make_fake_getitem(given_config)
        config.getint = self.make_fake_getint(given_config)
        exists.return_value = True

        expected_value = {
            'log_file': 'test_log_file_path',
            'ip_address': 'test_ip_address',
            'port': 'test_port',
            'login': 'test_login',
            'password': 'test_password',
            'database_directory': 'test_db_directory_path',
            'h_settings_db': 'test_settings_database',
            'data_db': 'test_database',
            'harvester_delay': 10,
            'update_delay': 300
        }
        test_value = read_settings()
        self.assertDictEqual(test_value, expected_value, 'Configuration parsed incorrectly!')
        del config

    @patch('sys.stderr')
    @patch('rssd4.worker.rssd4.os.path.exists')
    def test_config_not_exists(self, exists, mock_stderr):
        """Test what function raises exception if configuration
        file not found.
        """
        mock_stderr = open('/dev/null', 'a')  # suppress console output about not existing file
        exists.return_value = False
        self.assertRaises(FileNotFoundError, read_settings)

    def make_fake_getitem(self, config):
        def getitem(self, key):
            return config[key]
        return getitem

    def make_fake_getint(self, config):
        def getint(key1, key2):
            return config[key1][key2]
        return getint


class StartWorkerTestCase(unittest.TestCase):
    """Test for start_worker() function."""

    @patch('rssd4.worker.worker.main')
    def test_normal_operation(self, mock_worker_main):
        """Function should call rssd4.worker.worker.main() and exit clearly."""
        given_parameters = ('test_parameter1', 'test_parameter2')
        start_worker(*given_parameters)
        mock_worker_main.assert_called_once_with(*given_parameters)


class StartApiTestCase(unittest.TestCase):
    """Test for start_api() function."""

    @patch('rssd4.worker.rssd4.bottle.run')
    def test_normal_standalone(self, mock_bottle_run):
        """Function should acquire and release lock and run Bottle server."""
        lock = MagicMock()
        args = MagicMock()
        args.standalone = True
        settings = {
            'database_directory': None,
            'data_db': None,
            'h_settings_db': None,
            'ip_address': 'test_ip_address',
            'port': 'test_port',
            'login': 'test_login',
            'password': 'test_password'
        }
        with patch('rssd4.worker.rssd4.bottle.debug'), \
                patch('rssd4.worker.rssd4.logging.debug'), \
                patch('rssd4.worker.rssd4.Categories'), \
                patch('rssd4.worker.rssd4.Feeds'):
            result = start_api(lock, args, settings=settings)
            self.assertEqual(result, None)
        lock.acquire.assert_called_once_with()
        lock.release.assert_called_once_with()
        mock_bottle_run.assert_called_once_with(host=settings['ip_address'],
                                                port=settings['port'])

    @patch('rssd4.worker.rssd4.bottle.default_app')
    def test_normal_production(self, mock_default_app):
        """Function should acquire and release lock and run code for
        production server.
        """
        lock = MagicMock()
        args = MagicMock()
        args.standalone = False
        settings = {
            'database_directory': None,
            'data_db': None,
            'h_settings_db': None,
            'ip_address': 'test_ip_address',
            'port': 'test_port',
            'login': 'test_login',
            'password': 'test_password'
        }
        mock_default_app.return_value = 'test_bottle_run'
        with patch('rssd4.worker.rssd4.bottle.debug'), \
                patch('rssd4.worker.rssd4.logging.debug'), \
                patch('rssd4.worker.rssd4.Categories'), \
                patch('rssd4.worker.rssd4.Feeds'):
            result = start_api(lock, args, settings=settings)
            self.assertEqual(result, 'test_bottle_run')
        lock.acquire.assert_called_once_with()
        lock.release.assert_called_once_with()
        mock_default_app.assert_called_once_with()


class StartMainProgramTestCase(unittest.TestCase):
    """Test for start_main_program() function."""

    @patch('rssd4.worker.rssd4.logging')
    @patch('rssd4.worker.rssd4.multiprocessing.Lock')
    def test_normal_operation(self, mock_lock, mock_logging):
        """Function should acquire db_lock, set logging level
        and execute child process.
        """
        mock_worker_lock = MagicMock()
        mock_lock.return_value = mock_worker_lock
        mock_args = MagicMock()
        mock_args.debug = False
        mock_settings = {'log_file': 'test_log_file_path'}
        mock_logging.INFO = 123

        expected_logging_settings = {
            'format': '%(asctime)s:%(levelname)s:%(message)s',
            'datefmt': '%Y-%m-%dT%H:%M:%S%z',
            'filename': 'test_log_file_path',
            'level': 123
        }

        with patch('rssd4.worker.rssd4.multiprocessing.Process') as process, \
                patch('rssd4.worker.rssd4.start_api'):
            child_process = MagicMock()
            process.return_value = child_process
            start_main_program(mock_args, mock_settings)
            child_process.start.assert_called_once_with()
        mock_worker_lock.acquire.assert_called_once_with()
        mock_logging.basicConfig.assert_called_once_with(**expected_logging_settings)

    @patch('rssd4.worker.rssd4.logging')
    @patch('rssd4.worker.rssd4.multiprocessing.Lock')
    def test_debug_operation(self, mock_lock, mock_logging):
        """Function should acquire db_lock, set logging level
         and execute child process.
         """
        mock_worker_lock = MagicMock()
        mock_lock.return_value = mock_worker_lock
        mock_args = MagicMock()
        mock_args.debug = True
        mock_settings = {'log_file': 'test_log_file_path'}
        mock_logging.DEBUG = 456

        expected_logging_settings = {
            'format': '%(asctime)s:%(levelname)s:%(message)s',
            'datefmt': '%Y-%m-%dT%H:%M:%S%z',
            'filename': 'test_log_file_path',
            'level': 456
        }

        with patch('rssd4.worker.rssd4.multiprocessing.Process') as process, \
                patch('rssd4.worker.rssd4.start_api'):
            child_process = MagicMock()
            process.return_value = child_process
            start_main_program(mock_args, mock_settings)
            child_process.start.assert_called_once_with()
        mock_worker_lock.acquire.assert_called_once_with()
        mock_logging.basicConfig.assert_called_once_with(**expected_logging_settings)


if __name__ == '__main__':
    unittest.main()
