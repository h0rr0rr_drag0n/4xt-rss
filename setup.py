from distutils.core import setup


setup_settings = {
    'name': 'rssd4',
    'version': '0.1',
    'author': 'Eugene Andrienko',
    'author_email': 'h0rr0rrdrag@gmail.com',
    'url': 'https://github.com/h0rr0rrdrag0n/4xt-rss',
    'description': 'Daemon for RSS Reader, optimized for small computers.',
    'license': 'MIT',
    'packages': ['rssd4', 'rssd4.api', 'rssd4.harvesters', 'rssd4.worker'],
    'scripts': ['startrssd4'],
    'data_files': [('/etc', ['configs/rssd4.conf', 'configs/rssd4-uwsgi.ini']),
                   ('/etc/nginx', ['configs/rssd4-nginx.conf'])],
    'classifiers': [
        'Environment :: No Input/Output (Daemon)',
        'Framework :: Bottle',
        'Development Status :: 3 - Alpha',
        'Intended Audience :: System Administrators',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.4',
        'Operating System :: POSIX :: Linux',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application'
    ],
    'keywords': ['rss'],
    'long_description': """\
    There is daemon for RSS Reader, optimized for small computers.
    --------------------------------------------------------------

    This daemon optimized for small computers and other calculators,
    like Raspberry Pi.
    """
}

setup(**setup_settings)
