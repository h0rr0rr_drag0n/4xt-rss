[![Build
Status](https://travis-ci.org/h0rr0rrdrag0n/4xt-rss.svg?branch=develop)](https://travis-ci.org/h0rr0rrdrag0n/4xt-rss) [![Coverage Status](https://coveralls.io/repos/h0rr0rrdrag0n/4xt-rss/badge.svg?branch=develop&service=github)](https://coveralls.io/github/h0rr0rrdrag0n/4xt-rss?branch=develop) [![Code Health](https://landscape.io/github/h0rr0rrdrag0n/4xt-rss/develop/landscape.svg?style=flat)](https://landscape.io/github/h0rr0rrdrag0n/4xt-rss/develop)

# 4xt-rss

4xt-rss (reads as tiny-tiny-tiny-tiny rss) designed as RSS reader with
web-interface for small computers, like Raspberry Pi and other calculators.
Application uses Python3, SQLite and any web-server, which can work with
uWSGI.

## Requirements

You need to have Python 3.3 or higher and someone Web-server, which will pass
API calls to rssd4 daemon via uWSGI. Required Python packages are listed in the
`requirements.txt` file.

You **should** have a SSL key to organize HTTPS access to your Web-server.
Instead, password(s) (HTTP Basic authentication) sent to application via plain HTTP will be
**insecure** and possibly **compromised**!

At present time, application tested under the Nginx, but you feel free to use any other
Web-server, if you can configure it. Please, send me pull request with sample
configuration file (and some useful notes) if all going ok.

## Installation

You have 2 options to install rssd4 daemon:

* Clone this repository and run:
```
python setup.py build
sudo python setup.py install
```
* Take any zip-archive from "Releases" page in this repository and unpack it to the /
catalog.

## Configuration

By default, all configuration files will be copied to the /etc/ catalog. In
FreeBSD you should use ``/usr/local/etc/`` catalog and ``setup-freebsd.py``
installation script.

Edit rssd4.conf configuration file, before start using RSSD4.

### Web-server configuration

#### Nginx

Look at the ``configs/rssd4-nginx.conf`` - sample configuration file for Nginx. Paste
it to the your Nginx configuration file. Or edit your ``nginx.conf`` like this:
```
http {
   # some lines
   include rssd4-nginx.conf
   # some lines too
}
```

### uWSGI configuration

uWSGI config-file will be copied to the ``/etc/rssd4-uwsgi.ini``. Use command ``uwsgi --ini /etc/rssd4-uwsgi.ini``
to start uWSGI (and/or paste it somewhere to ``/etc/rc.local``).

### RSSD4 configuration

Look to the ``/etc/rssd4.conf`` or ``configs/rssd4.conf``.

